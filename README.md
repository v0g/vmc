Vmc - Virtual machine creator
=============================

Vmc creates vanilla Debian or CentOS virtual machines directly from
the official ISO images, to be further provisioned though SSH.


Description
===========

Management of the VMs is straight forward via `systemctl` and does not
require `root` privileges on the host system.  Vmc adds appropriate
user-configuration entries for `systemd` and `ssh`, which work well
as-is, but can be easily adjusted to specific needs.  Vmc also
provides a clean removal mechanism.

The installation is unattended, headless and very fast.  Intermediate
VM images are cached, so the creation of a second and third VM is even
faster.  All downloads are fully verified, proper trust anchors are
provided.  No additional packages besides the SSH server are
installed.  The created VM image is portable: It runs on QEMU/KVM,
VirtualBox and many other hypervisors.

Vmc provides many sane defaults, so it only needs to ask for few
parameters.  Those are sufficient to provide a useful VM, which can
be adjusted to special needs afterwards.


Internals
=========

The GRUB timeout is disabled, so the VM doesn't wait 5 seconds on
every reboot for its imaginary interactive user.  The SSH server
accepts only your own SSH public keys, which are automatically
collected from `~/.ssh/`.  Login by password is forbidden, but feel
free to change this if you must.  Network is configured for DHCP, feel
free to change it to a static configuration.  The default timezone is
UTC, feel free to change it if you must.  The default keyboard layout
is the US layout, which may be debatable, but is irrelevant for SSH
anyway.  The `upower` package is installed for proper shutdown on ACPI
poweroff signal.

There is one more service added to the VM, which is somewhat special
but very lightweight and extremely useful: On boot, the partition and
filesystem sizes are adjusted automatically whenever the VM image was
enlarged.  That way, adding more disk space is really as simple as
`qemu-img resize` or `lvresize`, followed by restarting the VM.


Installation
============

Installation into a user's home directory is straight forward, using
standard locations and assuming that `~/.local/bin` is in `$PATH`:

    git clone https://gitlab.com/v0g/vmc.git ~/.local/share/vmc
    mkdir -p ~/.local/bin
    ln -s ../share/vmc/vmc ~/.local/bin/vmc

Don't forget to upgrade from time to time:

    vmc upgrade

The latter is a convenience wrapper for `git pull --ff-only`.


Usage: Create VM image
======================

Create a CentOS VM for VirtualBox:

    vmc create centos test1.vdi

- `centos`: Operating system (`debian` or `centos`)
- `test1.vdi`: Image file to create (`*.raw` or `*.vdi`)


Usage: Create managed user VM
=============================

Create and automatically run a Debian VM with QEMU/KVM:

    vmc setup debian test2 22000

- `debian`: Operating system (`debian` or `centos`)
- `test2`: VM name
- `22000`: Port number on localhost for SSH, must be different for each VM!

Edit the following file to change the VM's QEMU settings, for example,
to provide more RAM or to forward more network services:

    ~/.local/share/systemd/user/test2.service

Local `systemd` and `ssh` configuration files are generated
automatically, so the VM can be accessed and managed purely with
standard tools:

- `ssh test2`: Enter VM
- `ssh test2 poweroff`: Shutdown VM gracefully
- `systemctl stop test2`: Forcefully stop VM immediately
- `systemctl start test2`: Start VM
- `systemctl status test2`: Check whether VM is running
- `qemu-resize ~/.local/share/vmc/test2.qcow2 500G`: Enlarge VM image (to 500 GiB)

Note that a new VM image size or new QEMU settings are only effective
after restarting the VM.


Usage: Destroy managed user VM
==============================

Destroy the VM image as well as its `ssh` and `systemd` integration:

    vmc destroy test2
